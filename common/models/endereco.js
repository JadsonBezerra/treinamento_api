'use strict';

module.exports = function(Endereco) {
    Endereco.observe('before save',(endereco,cb)=>{
        if(endereco.instance.cep[0]!=='5') throw new Error('cep nao começa com 5')
        cb(null,endereco.instance)
    })
};
